export class Ride {
  name: string;
  imageUrl: string;
  category: string;
  level: string;
  focusAreas: string[] | [];
  rating: number | string;
}
