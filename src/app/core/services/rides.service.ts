import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map, retry, catchError } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Ride } from "../models/ride.model";

import { IMAGE_PLACEHOLDER_URL } from "../../constants.js";

function arrayToConfigObject(array) {
  return Object.fromEntries(array.map((item) => [item.id, item.name]));
}
@Injectable()
export class RidesService {
  private rides: Array<Ride> = [];

  constructor(private http: HttpClient) {}

  getRides(): Observable<Ride[]> {
    return this.http.get(process.env.SERVER_API_URL).pipe(
      map((res: any) => {
        const { categories, focusAreas, levels, workouts } = res;
        return {
          categoriesConfig: arrayToConfigObject(categories),
          focusAreasConfig: arrayToConfigObject(focusAreas),
          levelsConfig: arrayToConfigObject(levels),
          workouts,
        };
      }),
      map(({ categoriesConfig, focusAreasConfig, levelsConfig, workouts }) => {
        return workouts.map((workout) => {
          return {
            name: workout.name,
            imageUrl: workout.image?.url ?? IMAGE_PLACEHOLDER_URL,
            category: categoriesConfig[workout.categoryId],
            level: levelsConfig[workout.levelId],
            focusAreas:
              workout.focusAreasIds.length > 0
                ? workout.focusAreasIds.map(
                    (focusAreaId) => focusAreasConfig[focusAreaId]
                  )
                : [],
            rating: workout.rating?.toFixed(2) ?? "No results",
          };
        });
      })
    );
  }
}
