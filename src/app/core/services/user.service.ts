import { Injectable } from "@angular/core";
import { User } from "../models/user.model";
import * as ApplicationSettings from "@nativescript/core/application-settings";

@Injectable()
export class UserService {
  getFullName(): string {
    const { firstName, lastName } = JSON.parse(
      ApplicationSettings.getString("account", "{}")
    );
    return `${firstName} ${lastName}`;
  }

  register(user: User) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const account = JSON.stringify(user);
        ApplicationSettings.setString("account", account);
        resolve("success");
      }, 3000);
    });
  }

  login(user: User) {
    return new Promise((resolve, reject) => {
      const account = JSON.parse(
        ApplicationSettings.getString("account", "{}")
      );
      if (user.email === account.email && user.password === account.password) {
        ApplicationSettings.setBoolean("isAuthenticated", true);
        setTimeout(() => {
          resolve("success");
        }, 3000);
      } else {
        setTimeout(() => {
          reject(new Error("Unfortunately we could not find your account."));
        }, 3000);
      }
    });
  }

  logout() {
    ApplicationSettings.remove("isAuthenticated");
  }
}
