import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import {
  NativeScriptModule,
  NativeScriptFormsModule,
} from "@nativescript/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { HttpClientModule, HttpClient } from "@angular/common/http";

import { UserService } from "./core/services/user.service";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./features/login/login.component";
import { RegisterComponent } from "./features/register/register.component";
import { RidesComponent } from "./features/rides/rides.component";

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    RidesComponent,
  ],
  providers: [UserService, HttpClient],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
