import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { Page } from "@nativescript/core/ui";
import * as ApplicationSettings from "@nativescript/core/application-settings";
import { User } from "../../core/models/user.model";
import { UserService } from "../../core/services/user.service";

@Component({
  moduleId: module.id,
  selector: "ns-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {
  user: User;
  isLoading = false;

  public constructor(
    private page: Page,
    private userService: UserService,
    private router: RouterExtensions
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.page.actionBarHidden = true;
  }

  register(): void {
    if (
      !this.user.email?.trim() ||
      !this.user.firstName?.trim() ||
      !this.user.lastName?.trim() ||
      !this.user.password?.trim() ||
      !this.user.confirmPassword?.trim()
    ) {
      this.alert("All Fields are Required!");
      return;
    }
    if (this.user.password !== this.user.confirmPassword) {
      this.alert("Your passwords don't match.");
      return;
    }
    this.isLoading = true;
    this.userService
      .register(this.user)
      .then(() => {
        this.isLoading = false;
        this.alert("You have been successfully registered.");
      })
      .then(() => {
        this.router.navigate(["/login"]);
      })
      .catch((error) => {
        this.isLoading = false;
        this.alert(error.message);
      });
  }

  alert(message: string) {
    return alert({
      title: process.env.APP_NAME,
      message: message,
      okButtonText: "OK",
    });
  }
}
