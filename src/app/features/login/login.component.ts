import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { Page } from "@nativescript/core/ui";
import * as ApplicationSettings from "@nativescript/core/application-settings";
import { User } from "../../core/models/user.model";
import { UserService } from "../../core/services/user.service";

@Component({
  moduleId: module.id,
  selector: "ns-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  user: User;
  isLoading = false;

  public constructor(
    private page: Page,
    private userService: UserService,
    private router: RouterExtensions
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.page.actionBarHidden = true;
    if (ApplicationSettings.getBoolean("isAuthenticated", false)) {
      this.router.navigate(["/rides"], { clearHistory: true });
    }
  }

  login() {
    if (!this.user.email?.trim() || !this.user.password?.trim()) {
      this.alert("All Fields are Required!");
      return;
    }
    this.isLoading = true;
    this.userService
      .login(this.user)
      .then(() => {
        this.isLoading = false;
        this.router.navigate(["/rides"], { clearHistory: true });
      })
      .catch((error) => {
        this.isLoading = false;
        this.alert(error.message);
      });
  }

  alert(message: string) {
    return alert({
      title: process.env.APP_NAME,
      message: message,
      okButtonText: "OK",
    });
  }
}
