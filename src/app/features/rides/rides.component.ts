import * as ApplicationSettings from "@nativescript/core/application-settings";
import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";

import { Ride } from "../../core/models/ride.model";
import { RidesService } from "../../core/services/rides.service";
import { UserService } from "../../core/services/user.service";

@Component({
  moduleId: module.id,
  selector: "ns-rides",
  templateUrl: "./rides.component.html",
  styleUrls: ["./rides.component.scss"],
  providers: [RidesService],
})
export class RidesComponent implements OnInit {
  rides: Ride[];
  userName: string;
  isLoading = false;

  public constructor(
    private router: RouterExtensions,
    private ridesService: RidesService,
    private userService: UserService
  ) {}

  public ngOnInit() {
    if (!ApplicationSettings.getBoolean("isAuthenticated", false)) {
      this.router.navigate(["/login"], { clearHistory: true });
    }
    this.isLoading = true;
    this.userName = this.userService.getFullName();
    this.ridesService.getRides().subscribe(
      (rides) => {
        this.rides = rides;
        this.isLoading = false;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public logout() {
    this.userService.logout();
    this.router.navigate(["/login"], { clearHistory: true });
  }
}
